#include <dirent.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <time.h>


#include "networktools.h"
#include "networktools.h"

#ifndef IP_ADDRESS
#define IP_ADDRESS          "192.168.1.106"
#endif

#ifndef PORT_NUM
#define PORT_NUM            48631
#endif

#ifndef PROTOCOL
#define PROTOCOL            0
#endif

int main()
{

    int socketFD, msgLength, closerError, errsrv;
    int i, j, retVal, numErrors;
    ssize_t writeError, readError;
    struct sockaddr_in serverAddress;
    char *msg, fName[64], rcvMsg[16], msgBuffer[16];


/*
    // Setting up the server address
    memset((char *)&serverAddress, '\0', sizeof(serverAddress)); // Zero out the memory for the address
    serverAddress.sin_family = AF_INET; // Create a network-capable socket
    serverAddress.sin_port = htons(PORT_NUM); // Create the network port for the server
    serverAddress.sin_addr.s_addr = inet_addr(IP_ADDRESS); // Open a connection to the IP address of the server
*/

    serverAddress = CreateConnector(IP_ADDRESS, PORT_NUM);

       
    // Create the socket. AF_INET specifies normal internet based IP addresses.
    // SOCK_STREAM specifies a continuous stream requiring a TCP connection. 
    // SOCK_DGRAM would specify a UDP connection.
    socketFD = CreateSocket(0);
    

    // Connect the socket to the server at the IP address
    if(connect(socketFD, (struct sockaddr *)&serverAddress, sizeof(serverAddress)) < 0)
    {
        perror("Error connecting");
        exit(1);
    }

    sprintf(msgBuffer, "gs %d\r", 1);
    SendMessage(socketFD, msgBuffer);
    ReceiveMessage(socketFD, rcvMsg);

    fprintf(stdout, "%s\n", rcvMsg);


   return 0;

}