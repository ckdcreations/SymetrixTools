#!/usr/bin/env python3

import socket

from osc4py3.as_eventloop import *
from osc4py3 import oscbuildparse
from osc4py3 import oscmethod as osm

port = 55555
addr = ('', port)

serverSock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serverSock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
serverSock.bind(addr)
serverSock.listen(1)

osc_startup()

loop = 0

while loop == 0:

    print('Waiting for command...')

    # Receive Connection from Symetrix Device
    clientSock, clientAddr = serverSock.accept()
    print('connection from: ', clientAddr)
    data = clientSock.recv(64)
    print( 'data: ', data.decode() )
    

    if data.decode() == 'connect':

        print('Connected to Symetrix')

        # Create new socket to connect with device
        symetrixSender = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        symetrixSender.connect( (clientAddr[0], 48631) ) # Port 48631 for Symetrix comms

        # Send message to activate button signifying connection
        msg = b'CS 2 65536\r'
        symetrixSender.send(msg)

        # Confirm connection
        data = symetrixSender.recv(64)
        if data.decode() == 'ACK\r':
            newLoop = 0

            while newLoop == 0:
                data = symetrixSender.recv(64)
                print( 'data: ', data.decode()  )

    if data.decode() == 'quit':
        print('Exiting Program!')
        loop = 1

clientSock.close()
