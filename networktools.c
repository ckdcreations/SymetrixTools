#include <stdlib.h>
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include "networktools.h"

/******************************************************************************************************
 * Function: ValidateIPAddress
 * Description: Determines if the input string is a valid IPV4 address.
 * Inputs: A string
 * Outputs: Returns 1 if valid, 0 if invalid, -1 if there's an error
 *****************************************************************************************************/
int ValidateIPAddress(char * ipAddr)
{
    // inet_pton converts a text string into a binary numeric network address.
    // it will return 1 on a success, 0 on invalid, or -1 on errors.
    struct sockaddr_in addrCheck;
    int result = inet_pton(AF_INET, ipAddr, &(addrCheck.sin_addr));
    return result;
}

/******************************************************************************************************
 * Function: CreateConnector
 * Description: Creates a connection to an IPV4 network address
 * Inputs: String for IPV4 address, int port number
 * Outputs: Creates the connection and returns the serverAddress
 *****************************************************************************************************/
struct sockaddr_in CreateConnector(char* ipAddr, int portNum)
{
    struct sockaddr_in serverAddress;
    memset((char *)&serverAddress, '\0', sizeof(serverAddress));
    serverAddress.sin_family = AF_INET;
    serverAddress.sin_port = htons(portNum);
    serverAddress.sin_addr.s_addr = inet_addr(ipAddr);
    return serverAddress;
}

/******************************************************************************************************
 * Function: CreateSocket
 * Description: Opens a socket on the host machine
 * Inputs: int for protocol. 0 is for TCP 1 is for UDP
 * Outputs: Returns a socket file descriptor for further use
 *****************************************************************************************************/
int CreateSocket(int protocol)
{
    int socketFD;
    if(protocol == 0)
    {
        if( (socketFD = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        {
            perror("Error opening socket");
            exit(2);
        }
    }
    else if(protocol == 1)
    {
        if( (socketFD = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
        {
            perror("Error opening socket");
            exit(2);
        }
    }

    return socketFD;
}

/******************************************************************************************************
 * Function: SendMessage
 * Description: Sends a message to the remote server
 * Inputs: int for socket number, char string for message
 * Outputs: If error, will exit program
 *****************************************************************************************************/
void SendMessage(int socketFD, char msg[8])
{
    int errsrv;
    int buf = strlen(msg);
    int writeError = write(socketFD, msg, buf);

    // If there is an error writing the message to the socket, or if the message is incomplete, exit with error.
    if(writeError < 0)
    {
        errsrv = errno;
        fprintf(stderr, "Write error with code: %d\n", errsrv);
        exit(3);
    }
    else if(writeError >= 0 && writeError != buf)
    {
        perror("Write error: Incomplete message sent.\n");
        exit(3);
    }
}



/******************************************************************************************************
 * Function: ReceiveMessage 
 * Description: Receives a message to the remote server
 * Inputs: int for socket number, char string for message
 * Outputs: If error, will exit program
 *****************************************************************************************************/
void ReceiveMessage(int socketFD, char rcv[8])
{
        int errsrv, readError;
        readError = read(socketFD, rcv, 8);
        if(readError < 0)
        {
            errsrv = errno;
            fprintf(stderr, "Read error with code: %d\n", errsrv);
            exit(4);
        }
}




