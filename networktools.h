#ifndef _NETWORKTOOLS_H_
#define _NETWORKTOOLS_H_

void SendMessage(int socketFD, char msg[8]);
void ReceiveMessage(int socketFD, char rcv[8]);
int CreateSocket(int protocol);
int ValidateIPAddress(char * ipAddr);
struct sockaddr_in CreateConnector(char* ipAddr, int portNum);




#endif /* _NETWORKTOOLS_H_ */